import tkMessageBox, tkSimpleDialog, tkFileDialog
from initialization import *
from representations import *
from interface import *
from align import *
from interface_movie import *


def __init__(self):
    self.menuBar.addmenuitem('Plugin', 'command','cabsDock',
                             label = 'CABSdock plugin',
                             command = lambda s = self: job_dialog(s))


def job_dialog(app):

    cabsdock_job = None
    db = None

    local_complex = None
    rec_chains = None
    pept_chains = None

    local_ref = None
    ref_filepath = None
    ref_pdb_code = None

    complex_filepaths = None
    complex_pdb_code = None
    complex_name = None

    mode = tkSimpleDialog.askstring('Choose the type of structure to load',
                                    'Do you want to load complex structure, complex structure together with its '
                                    'reference structure or reference structure? '
                                    'Type "complex", "complex with reference" or "reference".',
                                    parent = app.root)

    if mode == 'complex' or mode == 'complex with reference':

        """
        #########################################################################
                       Gathering the data for loading a complex
        #########################################################################
        """

        local_complex = tkMessageBox.askquestion(message = 'Do you want to load local .pdb files/file with your '
                                                           'complex structure?',
                                                 parent = app.root)

        if local_complex == 'yes':
            complex_filepaths = tkFileDialog.askopenfilenames(title = 'Choose file/files',
                                                              initialdir = os.getcwd(),
                                                              defaultextension = '.pdb')

        else:
            cabsdock_job = tkMessageBox.askquestion(message = 'Do you want to load a CABSdock job from server?',
                                                    parent = app.root)

            if cabsdock_job == 'yes':
                job_id = tkSimpleDialog.askstring('Load CABSdock job from server',
                                                  'Please enter jobID:',
                                                  parent = app.root)

                if job_id is None:
                    return None

                job = download_job(job_id)

                if job is None:
                    tkMessageBox.showerror(title = 'Error while downloading CABSdock job',
                                           message = 'Job with given ID does not exist. Please check if ID is correct.')
                    return None

            else:
                db = tkMessageBox.askquestion(message = 'Do you want to load a .pdb file with complex structure from RCSB PDB?',
                                              parent = app.root)

                if db == 'yes':
                    complex_pdb_code = tkSimpleDialog.askstring('Load a complex file from RCSB PDB',
                                                                 'Please enter a PDB code of your complex structure:',
                                                                 parent = app.root)

                    if complex_pdb_code is None:
                        return None

            if cabsdock_job == 'no' and db == 'no':
                tkMessageBox.showerror(title = 'Error while loading complex files/file online',
                                       message = 'At the moment the plugin is only capable of loading online either '
                                                 'CABSdock job files or .pdb files/file from RCSB PDB. '
                                                 'We are sorry for any inconvenience this may have caused.')
                return None

        complex_name = tkSimpleDialog.askstring('Name your complex',
                                                'Please enter the name for the complex being loaded:',
                                                parent = app.root)

        if complex_name is None:
            return None

        rec_chains = tkSimpleDialog.askstring('Receptor chains',
                                              'Please enter receptor chain ids:',
                                              parent = app.root)

        if rec_chains is None:
            return None

        pept_chains = tkSimpleDialog.askstring('Peptide chains',
                                               'Please enter peptide chain ids:',
                                               parent=app.root)

        if pept_chains is None:
            return None

    if mode == 'complex with reference' or mode == 'reference':
        local_ref = tkMessageBox.askquestion(message = 'Do you want to load a local .pdb file with your reference '
                                                       'structure?',
                                             parent = app.root)

        """
        ##################################################################
                Gathering the data for loading reference structure
        ##################################################################
        """

        if local_ref == 'yes':
            ref_filepath = tkFileDialog.askopenfilename(title = 'Choose a file',
                                                        initialdir = os.getcwd(),
                                                        defaultextension = '.pdb')

        else:
            ref_pdb_code = tkSimpleDialog.askstring('Load a reference structure file from RCSB PDB',
                                                    'Please enter a PDB code of your reference structure:',
                                                    parent = app.root)

            if ref_pdb_code is None:
                return None

        if mode == 'reference':
            complex_name = tkSimpleDialog.askstring('Enter the name of the complex',
                                                    'Please enter the name of the loaded complex, which reference '
                                                    'structure you want to load:',
                                                    parent = app.root)

            if complex_name is None:
                return None

    """
    #################################################
                Loading complex structures
    #################################################
    """

    if mode == 'complex' or mode == 'complex with reference':

        if local_complex == 'yes':
            load_complex(rec_chains, pept_chains, complex_name,
                         fpaths = complex_filepaths)

        else:

            if cabsdock_job == 'yes':
                load_complex(rec_chains, pept_chains, complex_name,
                             partial_fname = 'model')

            else:
                load_complex(rec_chains, pept_chains, complex_name,
                             pdb = complex_pdb_code)

    """
    ##################################################
                Loading reference structure
    ##################################################
    """

    if mode == 'complex with reference' or mode == 'reference':

        if local_ref == 'yes':
            load_ref(complex_name,
                     fpath = ref_filepath)

        else:
            load_ref(complex_name,
                     pdb = ref_pdb_code)