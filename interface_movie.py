from pymol import cmd, stored
import os
from subprocess import call
from yrb import yrb
from interface import get_interface
from complex import get_complex
from initialization import complexes


def color_objects(model):

    get_interface(model, get_complex(model, complexes), cutoff = 1.)

    stored.receptor_interface_residues = list()
    stored.peptide_interface_residues = list()

    cmd.iterate(model + '_rec_if & (name ca)', 'stored.receptor_interface_residues.append(resi)')
    cmd.iterate(model + '_pept_if & (name ca)', 'stored.peptide_interface_residues.append(resi)')

    receptor_interface_residues_selection = '('

    for i in stored.receptor_interface_residues:
        receptor_interface_residues_selection += 'resi ' + i
        if stored.receptor_interface_residues.index(i) != len(stored.receptor_interface_residues) - 1:
            receptor_interface_residues_selection += ' or '
        else:
            receptor_interface_residues_selection += ')'

    peptide_interface_residues_selection = '('

    for i in stored.peptide_interface_residues:
        peptide_interface_residues_selection += 'resi ' + i
        if stored.peptide_interface_residues.index(i) != len(stored.peptide_interface_residues) - 1:
            peptide_interface_residues_selection += ' or '
        else:
            peptide_interface_residues_selection += ')'

    '''
    ################################################################
                     Pre-movie model visualization
    ################################################################
    '''

    cmd.disable(model)
    cmd.enable('_receptor_' + model)
    cmd.enable('_peptide' + model)
    cmd.orient('all')
    cmd.show_as('surface')

    yrb()

    cmd.color('grey60', '_receptor_' + model + ' and not ' + receptor_interface_residues_selection)
    cmd.color('grey80', '_peptide_' + model + ' and not ' + peptide_interface_residues_selection)


def set_movie_settings(model):

    cmd.set('matrix_mode', 1)
    cmd.set('movie_panel', 1)
    cmd.set('scene_buttons', 1)
    cmd.set('cache_frames', 0)

    for x in range(1):
        cmd.madd('1 x60')
        cmd.frame(1000)
        cmd.rotate('y', -90, object = '_receptor_' + model)
        cmd.translate([-15, 0, 0], object = '_receptor_' + model)
        cmd.rotate('y', 100, object = '_peptide_' + model)
        cmd.translate([20, 0, 15], object = '_peptide_' + model)
        cmd.mview('store', object = '_receptor_' + model)
        cmd.mview('store', object = '_peptide_' + model)
        cmd.mview('store')
        cmd.mview('interpolate', object = '_receptor_' + model)
        cmd.mview('interpolate', object = '_peptide_' + model)
        cmd.mview('reinterpolate')

        # wait 0.5 second

        cmd.madd('1 x15')
        cmd.frame(1000)
        cmd.mview('store', object = '_receptor_' + model)
        cmd.mview('store', object = '_peptide_' + model)
        cmd.mview('store')
        cmd.mview('interpolate', object = '_receptor_' + model)
        cmd.mview('interpolate', object = '_peptide_' + model)
        cmd.mview('reinterpolate')

        cmd.madd('1 x60')
        cmd.frame(1000)
        cmd.translate([15, 0, 0], object = '_receptor_' + model)
        cmd.rotate('y', 90, object = '_receptor_' + model)
        cmd.translate([-20, 0, -15], object = '_peptide_' + model)
        cmd.rotate('y', -100, object = '_peptide_' + model)
        cmd.mview('store', object = '_receptor_' + model)
        cmd.mview('store', object = '_peptide_' + model)
        cmd.mview('store')
        cmd.mview('interpolate', object = '_receptor_' + model)
        cmd.mview('interpolate', object = '_peptide_' + model)
        cmd.mview('reinterpolate')

        # wait 0.5 second

        cmd.madd('1 x15')
        cmd.frame(1000)
        cmd.mview('store', object = '_receptor_' + model)
        cmd.mview('store', object = '_peptide_' + model)
        cmd.mview('store')
        cmd.mview('interpolate', object = '_receptor_' + model)
        cmd.mview('interpolate', object = '_peptide_' + model)
        cmd.mview('reinterpolate')


def set_movie_visualization():

    cmd.set('antialias', 1)
    cmd.set('depth_cue', 0)
    cmd.set('ambient', 0.1)
    cmd.set('specular', 0.3)
    cmd.set('spec_count', 1)
    cmd.set('light_count', 8)
    cmd.set('direct', 0.75)
    cmd.bg_color('white')
    cmd.set('surface_quality', 2)
    cmd.set('ray_shadow_decay_factor', 0.1)
    cmd.set('ray_shadow_decay_range', 2)
    cmd.set('ambient_occlusion_mode', 1)
    cmd.set('ambient_occlusion_scale', 15)

    cmd.set('hash_max', 220)
    cmd.set('ray_trace_mode', 0)
    cmd.set('depth_cue', 0)
    cmd.set('ray_trace_fog', 0)
    cmd.set('ray_trace_frames', 1)
    cmd.set('cache_frames', 0)


def prepare_interface_movie(model):

    color_objects(model)
    cmd.disable('all')
    cmd.enable('_receptor_' + model)
    cmd.enable('_peptide_' + model)


def get_interface_movie(model, movie_fname):

    set_movie_settings(model)
    set_movie_visualization()

    directory = movie_fname

    if not os.path.exists(directory):
        os.makedirs(directory)

    os.chdir(directory)

    for i in range(150):
        cmd.frame(i)
        cmd.png('%03i' % i, width = 1280, height = 720, dpi = 600, ray = 1)

    #cmd.mpng(movie_fname)

    program = 'ffmpeg'
    args = ' -framerate 30 -pattern_type glob -i *.png -s:v 1280x720 -c:v libx264 -profile:v main -crf 12 ' \
           '-tune stillimage -pix_fmt yuv420p -preset veryslow -r 30 ' + movie_fname + '.mp4'

    call([program, ] + args.split())


cmd.extend('prepare_interface_movie', prepare_interface_movie)
cmd.extend('get_interface_movie', get_interface_movie)