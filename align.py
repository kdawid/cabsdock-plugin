from pymol import cmd
from complex import get_complex
from initialization import complexes


def align_chains(mobile = None, target = None, all = False, ref = False, complex_chain = None):

    """
    :param mobile: name of the model's PyMOL object followed by the id of the chain you want to align;
                   i.e., 'my_job_model_1 A' - chain A from the 'my_job_model_1' object;
                   - default: None

    :param target: name of the model's PyMOL object followed by the id of the chain to which you want to align;
                   i.e., 'my_job_model_7 A' - chain A from the 'my_job_model_7' object;
                   - default: None

    :param all: informs whether a mobile selection should consist of all models of the complex;
                - default: False

    :param ref: informs whether a target selection should be the complex's reference structure;
                - default: False

    :param complex_chain: if the parameters 'all' and 'ref' were set to True, the 'complex_chain' parameter should
                          contain the name of the complex currently being analyzed together with the chain by which you
                          want to align the structures;
                          i.e., 'my_job A' - chain A from the 'my_job' complex;
                          - default: None

    :return: performs structural alignment of the objects provided in the parameters
             calculates RMS of the alignment

    For more detailed information on parameters please go to https://bitbucket.org/kdawid/cabsdock-plugin/wiki/Home.
    """

    all = bool(all)
    ref = bool(ref)

    leave_enabled = list()

    """
    ##########################################################################################
                       Creating strings with mobile and target selections
    ##########################################################################################
    """

    if mobile is not None and target is not None:
        mobile = mobile.split()
        mobile_model = mobile[0]
        mobile_chains = mobile[1:]
        mobile_sel = mobile_model + ' and (chain '

        for chain in mobile_chains:
            mobile_sel += chain

            if mobile_chains.index(chain) == len(mobile_chains) - 1:
                mobile_sel += ')'

            else:
                mobile_sel += ' or chain '

        target = target.split()
        target_model = target[0]
        target_chains = target[1:]
        target_sel = target_model + ' and (chain '

        for chain in target_chains:
            target_sel += chain

            if target_chains.index(chain) == len(target_chains) - 1:
                target_sel += ')'

            else:
                target_sel += ' or chain '

        cmd.enable(target_model)
        cmd.enable(mobile_model)

        leave_enabled.append(target_model)
        leave_enabled.append(mobile_model)

    elif all is True and target is not None:
        models = list()
        mobile_sels = list()

        target = target.split()
        target_model = target[0]
        target_chains = target[1:]
        target_complex = get_complex(target_model, complexes)
        target_sel = target_model + ' and (chain '

        for chain in target_chains:
            target_sel += chain

            if target_chains.index(chain) == len(target_chains) - 1:
                target_sel += ')'

            else:
                target_sel += ' or chain '

        for filename in target_complex.filenames:
            if target_complex.name + '_' + filename != target_model:
                models.append(target_complex.name + '_' + filename)

        for model in models:
            mobile_sel = model + ' and (chain '

            for chain in target_chains:
                mobile_sel += chain

                if target_chains.index(chain) == len(target_chains) - 1:
                    mobile_sel += ')'

                else:
                    mobile_sel += ' or chain '

            mobile_sels.append(mobile_sel)
            cmd.enable(model)
            leave_enabled.append(model)

        cmd.enable(target_model)

        leave_enabled.append(target_model)

    elif ref is True and mobile is not None:
        mobile = mobile.split()
        mobile_model = mobile[0]
        mobile_chains = mobile[1:]
        mobile_complex = get_complex(mobile_model, complexes)
        mobile_sel = mobile_model + ' and (chain '

        for chain in mobile_chains:
            mobile_sel += chain

            if mobile_chains.index(chain) == len(mobile_chains) - 1:
                mobile_sel += ')'

            else:
                mobile_sel += ' or chain '

        ref_target = mobile_complex.name + '_ref'
        ref_sel = ref_target + ' and (chain '

        for chain in mobile_chains:
            ref_sel += chain

            if mobile_chains.index(chain) == len(mobile_chains) - 1:
                ref_sel += ')'

            else:
                ref_sel += ' or chain '

        cmd.enable(ref_target)
        cmd.enable(mobile_model)

        leave_enabled.append(ref_target)
        leave_enabled.append(mobile_model)

    elif all is True and ref is True:
        models = list()
        mobile_sels = list()
        complex = None

        complex_chain = complex_chain.split()
        complex_name = complex_chain[0]
        complex_chains = complex_chain[1:]

        for c in complexes:
            if c.name == complex_name:
                complex = c

        for filename in complex.filenames:
            models.append(complex.name + '_' + filename)

        for model in models:
            mobile_sel = model + ' and (chain '

            for chain in complex_chains:
                mobile_sel += chain

                if complex_chains.index(chain) == len(complex_chains) - 1:
                    mobile_sel += ')'

                else:
                    mobile_sel += ' or chain '

            mobile_sels.append(mobile_sel)
            cmd.enable(model)
            leave_enabled.append(model)

        ref_target = complex_name + '_ref'
        ref_sel = ref_target + ' and (chain '

        for chain in complex_chains:
            ref_sel += chain

            if complex_chains.index(chain) == len(complex_chains) - 1:
                ref_sel += ')'

            else:
                ref_sel += ' or chain '

        cmd.enable(ref_target)
        leave_enabled.append(ref_target)

    """
    ###############################################################
            Chains alignment and calculation of its RMS
    ###############################################################
    """

    if mobile is not None and target is not None:
        cmd.align(mobile_sel, target_sel)

        cmd.rms(mobile_sel, target_sel, quiet = 0)

    elif all is True and target is not None:
        for mobile_sel in mobile_sels:
            cmd.align(mobile_sel, target_sel)

        cmd.rms(mobile_sels[0], target_sel, quiet = 0)

    elif ref is True and mobile is not None:
        cmd.align(mobile_sel, ref_sel)

        cmd.rms(mobile_sel, ref_sel, quiet = 0)

    elif all is True and ref is True:
        for mobile_sel in mobile_sels:
            cmd.align(mobile_sel, ref_sel)

        cmd.rms(mobile_sels[0], ref_sel, quiet = 0)

    """
    ##############################
             Cleaning up
    ##############################
    """

    for obj in cmd.get_names('objects'):
        if obj in leave_enabled:
            cmd.enable(obj)

        else:
            cmd.disable(obj)

    cmd.orient('all')


def align_receptors(mobile = None, target = None, all = False, ref = False, complex_name = None):

    """
     :param mobile: name of the model's PyMOL object you want to align;
                    i.e., 'my_job_model_1'

     :param target: name of the model's PyMOL object to which you want to align;
                    i.e., 'my_job_model_7'

     :param all: informs whether a mobile selection should consist of all models of the complex;
                - default: False

    :param ref: informs whether a target selection should be the complex's reference structure;
                - default: False

    :param complex_name: if the parameters 'all' and 'ref' were set to True, the 'complex_name' parameter should contain
                         the name of the complex currently being analyzed;
                         i.e., 'my_job';
                         - default: None

    :return: performs structural alignment of the objects provided in the parameters
             calculates RMSD of the alignment

     For more detailed information on parameters please go to https://bitbucket.org/kdawid/cabsdock-plugin/wiki/Home.
     """

    all = bool(all)
    ref = bool(ref)

    leave_enabled = list()

    """
    ##########################################################################################
                       Creating strings with mobile and target selections
    ##########################################################################################
    """

    if mobile is not None and target is not None:

        mobile_complex = get_complex(mobile, complexes)
        mobile_rec_sel = mobile + ' and ' + mobile_complex.receptor_sel

        target_complex = get_complex(target, complexes)
        target_rec_sel = target + ' and ' + target_complex.receptor_sel

        cmd.enable(target)
        cmd.enable(mobile)

        leave_enabled.append(target)
        leave_enabled.append(mobile)

    elif all is True and target is not None:
        models = list()
        mobile_rec_sels = list()

        target_complex = get_complex(target, complexes)
        target_rec_sel = target + ' and ' + target_complex.receptor_sel

        cmd.enable(target)
        leave_enabled.append(target)

        for filename in target_complex.filenames:
            if target_complex.name + '_' + filename != target:
                models.append(target_complex.name + '_' + filename)

        for model in models:
            mobile_rec_sels.append(model + ' and ' + target_complex.receptor_sel)

            cmd.enable(model)
            leave_enabled.append(model)

    elif ref is True and mobile is not None:
        mobile_complex = get_complex(mobile, complexes)
        mobile_rec_sel = mobile + ' and ' + mobile_complex.receptor_sel

        ref_target = mobile_complex.name + '_ref'

        cmd.enable(ref_target)
        cmd.enable(mobile)

        leave_enabled.append(ref_target)
        leave_enabled.append(mobile)

    elif all is True and ref is True:
        models = list()
        mobile_rec_sels = list()
        complex = None

        for c in complexes:
            if c.name == complex_name:
                complex = c

        for filename in complex.filenames:
            models.append(complex_name + '_' + filename)

        for model in models:
            mobile_rec_sels.append(model + ' and ' + complex.receptor_sel)

            cmd.enable(model)
            leave_enabled.append(model)

        ref_target = complex_name + '_ref'

        cmd.enable(ref_target)
        leave_enabled.append(ref_target)

    """
    ################################################################
            Receptors alignment and calculation of its RMSD
    ################################################################
    """

    if mobile is not None and target is not None:
        cmd.align(mobile_rec_sel, target_rec_sel, quiet = 0)

    elif all is True and target is not None:
        for mobile_rec_sel in mobile_rec_sels:
            cmd.align(mobile_rec_sel, target_rec_sel)

        cmd.align(mobile_rec_sels[0], target_rec_sel, transform = 0, quiet = 0)

    elif ref is True and mobile is not None:
        cmd.align(mobile_rec_sel, ref_target, quiet = 0)

    elif all is True and ref is True:
        for mobile_rec_sel in mobile_rec_sels:
            cmd.align(mobile_rec_sel, ref_target)

        cmd.align(mobile_rec_sels[0], ref_target, transform = 0, quiet = 0)

    """
    ##############################
             Cleaning up
    ##############################
    """

    for obj in cmd.get_names('objects'):
        if obj in leave_enabled:
            cmd.enable(obj)

        else:
            cmd.disable(obj)

    cmd.orient('all')


def peptides_rmsd(mobile = None, target = None, all = False, ref = False, complex_name = None):

    """
    :param mobile: name of the model's PyMOL object which peptide you want to align;
                   i.e., 'my_job1_model_1'

    :param target: name of the model's PyMOL object to which peptide you want to align;
                   i.e., 'my_job2_model_1'

    :param all: informs whether a mobile selection should consist of all models of the complex;
                - default: False

    :param ref: informs whether a target selection should be the complex's reference structure;
                - default: False

    :param complex_name: if the parameters 'all' and 'ref' were set to True, the 'complex_name' parameter should contain
                         the name of the complex currently being analyzed;
                         i.e., 'my_job';
                         - default: None

    :return: performs structural alignment of the receptors from the objects provided in the parameters
             calculates RMSD of the alignment of the peptides from the objects provided in the parameters

    For more detailed information on parameters please go to https://bitbucket.org/kdawid/cabsdock-plugin/wiki/Home.
    """

    all = bool(all)
    ref = bool(ref)
    leave_enabled = list()

    """
    ##########################################################################################
                       Creating strings with mobile and target selections
    ##########################################################################################
    """

    if mobile is not None and target is not None:

        mobile_complex = get_complex(mobile, complexes)
        mobile_pep_sel = mobile + ' and ' + mobile_complex.peptide_sel

        target_complex = get_complex(target, complexes)
        target_pep_sel = target + ' and ' + target_complex.peptide_sel

        cmd.enable(target)
        cmd.enable(mobile)

        leave_enabled.append(target)
        leave_enabled.append(mobile)

    elif all is True and target is not None:
        models = list()
        mobile_pep_sels = list()

        target_complex = get_complex(target, complexes)
        target_pep_sel = target + ' and ' + target_complex.peptide_sel

        cmd.enable(target)
        leave_enabled.append(target)

        for filename in target_complex.filenames:
            if target_complex.name + '_' + filename != target:
                models.append(target_complex.name + '_' + filename)

        for model in models:
            mobile_pep_sels.append(model + ' and ' + target_complex.peptide_sel)

            cmd.enable(model)
            leave_enabled.append(model)

    elif ref is True and mobile is not None:
        mobile_complex = get_complex(mobile, complexes)
        mobile_pep_sel = mobile + ' and ' + mobile_complex.peptide_sel

        ref_target = mobile_complex.name + '_ref'

        cmd.enable(ref_target)
        cmd.enable(mobile)

        leave_enabled.append(ref_target)
        leave_enabled.append(mobile)

    elif all is True and ref is True:
        models = list()
        mobile_pep_sels = list()
        complex = None

        for c in complexes:
            if c.name == complex_name:
                complex = c

        for filename in complex.filenames:
            models.append(complex_name + '_' + filename)

        for model in models:
            mobile_pep_sels.append(model + ' and ' + complex.peptide_sel)

            cmd.enable(model)
            leave_enabled.append(model)

        ref_target = complex_name + '_ref'

        cmd.enable(ref_target)
        leave_enabled.append(ref_target)

    """
    ##########################################################################
               Receptors alignment and calculation of peptides RMSD
    ##########################################################################
    """

    if mobile is not None and target is not None:
        align_receptors(mobile = mobile, target = target)

        cmd.align(mobile_pep_sel, target_pep_sel, transform = 0, quiet = 0)

    elif all is True and target is not None:
        align_receptors(target = target, all = True)

        cmd.align(mobile_pep_sels[0], target_pep_sel, transform = 0, quiet = 0)

    elif ref is True and mobile is not None:
        align_receptors(mobile = mobile, ref = True)

        cmd.align(mobile_pep_sel, ref_target, transform = 0, quiet = 0)

    elif all is True and ref is True:
        align_receptors(all = True, ref = True, complex_name = complex_name)

        cmd.align(mobile_pep_sels[0], ref_target, transform = 0, quiet = 0)

    """
    ##############################
             Cleaning up
    ##############################
    """

    for obj in cmd.get_names('objects'):
        if obj in leave_enabled:
            cmd.enable(obj)

        else:
            cmd.disable(obj)

    cmd.orient('all')


cmd.extend('align_chains', align_chains)
cmd.extend('align_receptors', align_receptors)
cmd.extend('peptides_rmsd', peptides_rmsd)