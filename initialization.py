from pymol import cmd
import urllib, zipfile, os, threading
from time import sleep
from glob import glob
from complex import Complex


def download_job(jobid):

    """
    :param jobid: id of a CABSdock job;
                  i.e., 'a05cd27e43770e4'

    :return: downloads a CABSdock job from the 'jobid' parameter into the current directory

    For more detailed information on parameters please go to https://bitbucket.org/kdawid/cabsdock-plugin/wiki/Home.
    """

    host = 'http://biocomp.chem.uw.edu.pl/CABSdock/job/'
    dir_name = 'CABSdock_' + jobid
    msg = 'Downloading %s' % dir_name
    cmd.wizard('message', msg)
    cmd.wizard()

    """
    #####################################
            Checking the directory
    #####################################
    """

    if os.path.isdir(dir_name):
        sleep(1)
        os.chdir(dir_name)
        cmd.wizard('message', 'Done')
        cmd.wizard()

        return dir_name

    """
    #####################################
    """

    zip_name = dir_name + '.zip'
    url = host + zip_name
    u = urllib.urlopen(url)

    """
    #####################################
            Downloading the job
    #####################################
    """

    if u.getcode() is not 200:
        cmd.wizard('message', 'No such job: %s' % jobid)
        cmd.wizard()

        return None

    else:

        t = threading.Thread(target = urllib.urlretrieve, args = (url, zip_name))
        t.start()

        while t.isAlive():
            msg += '.'
            cmd.wizard('message', msg)
            cmd.wizard()
            sleep(1)

        zipfile.ZipFile(zip_name).extractall()
        os.remove(zip_name)
        os.chdir(dir_name)
        cmd.wizard('message', 'Done')
        cmd.wizard()

        return dir_name


"""
######################################################################
            Global list 'complexes' which will contain
          all the Complex objects representing complexes
                        loaded into PyMOL
######################################################################
"""

complexes = list()

"""
######################################################################
"""


def load_complex(receptor_chains, peptide_chains, complex_name,
                 fpaths = None, partial_fname = None, fname = None, path = None, pdb = None):

    """
    :param receptor_chains: chain ids/id of the complex's receptor;
                            i.e., 'A B' - if receptor consists of two chains
                            i.e., 'A' - if receptor consists of one chain

    :param peptide_chains: a chain id of the complex's peptide;
                           i.e., 'C'

    :param complex_name: name of the complex;
                         i.e., 'my_job'

    :param fpaths: list of filepaths/a filepath to the .pdb files/file which contain/contains complex's structure;
                   - PARAMETER UNAVAILABLE FROM PYMOL COMMAND LINE

    :param partial_fname: part of the filename which is common to all the .pdb files you want to load;
                          i.e., 'model' - if your files are named 'model_1', 'model_2' etc.;
                          - default: None

    :param fname: name of the .pdb file (without the .pdb extension) which contains your complex's structure;
                  i.e., '2FVJ';
                  - default: None

    :param path: a path to the directory where the file/files you want to load is/are located;
                 i.e., '/home/kdawid/work/CABSdock_a05cd27e43770e4';
                 - default: None

    :param pdb: PDB codes/code of the complex's structures/structure
                i.e., '2FVJ'
                - default: None

    :return: loads desired complex's structure/structures into PyMOL

    For more detailed information on parameters please go to https://bitbucket.org/kdawid/cabsdock-plugin/wiki/Home.
    """

    msg = 'Loading complex'
    cmd.wizard('message', msg)
    cmd.wizard()

    '''t = threading.Thread(target=urllib.urlretrieve, args=(url,zip_name))
    t.start()

    while t.isAlive():
        msg += '.'
        cmd.wizard('message', msg)
        cmd.wizard()
        sleep(1)'''

    receptor_chains = receptor_chains.split()
    peptide_chains = peptide_chains.split()

    global complexes
    complex = None

    """
    ################################################################################################
                Creating a Complex object/Choosing an already existing Complex object
    ################################################################################################
    """

    if len(complexes) != 0:
        for obj in complexes:
            if obj.name == complex_name:
                complex = obj
                break

    if complex is None:
        complex = Complex(complex_name, receptor_chains, peptide_chains)
        complexes.append(complex)

    """
    ###########################################
              Loading .pdb files/file
    ###########################################
    """

    if fpaths is not None:
        directory = os.path.dirname(fpaths[0])

        if os.getcwd() != directory:
            os.chdir(directory)

        for f in fpaths:
            file = os.path.basename(f)
            fname = os.path.splitext(file)[0]
            (complex.filenames).append(fname)
            name = complex_name + '_' + fname
            cmd.load(file, name)
            create_sel(complex)
            create_obj_and_repr(complex, name)

    elif partial_fname is not None:

        if path is not None:
            os.chdir(path)

        for f in glob(partial_fname + '*.pdb'):
            fname = os.path.splitext(f)[0]
            (complex.filenames).append(fname)
            name = complex.name + '_' + fname
            cmd.load(f, name)
            create_sel(complex)
            create_obj_and_repr(complex, name)

    elif fname is not None:

        if path is not None:
            os.chdir(path)

        (complex.filenames).append(fname)
        name = complex_name + '_' + fname
        cmd.load(fname + '.pdb', name)
        create_sel(complex)
        create_obj_and_repr(complex, name)

    elif pdb is not None:
        (complex.filenames).append(pdb)
        name = complex_name + '_' + pdb
        cmd.fetch(pdb, name)
        create_sel(complex)
        create_obj_and_repr(complex, name)

    """
    ###########################################
    """

    cmd.orient('all')

    cmd.wizard('message', 'Done')
    cmd.wizard()


def load_ref(complex_name,
             fpath = None, fname = None, path = None, pdb = None):

    """
    :param complex_name: name of the complex which reference structure you want to load;
                         i.e., 'my_job'

    :param fpath: a filepath to the .pdb file which contains reference structure;
                  - PARAMETER UNAVAILABLE FROM PYMOL COMMAND LINE

    :param fname: name of the .pdb file (without the .pdb extension) which contains reference structure;
                  i.e., '2FVJ';
                  - default: None

    :param path: a path to the directory where the file you want to load is located;
                 i.e., '/home/kdawid/work/structures';
                 - default: None

    :param pdb: a PDB code of the reference structure
                i.e., '2FVJ';
                - default: None

    :return: loads desired reference structure into PyMOL

    For more detailed information on parameters please go to https://bitbucket.org/kdawid/cabsdock-plugin/wiki/Home.
    """

    """
    ###############################################
              Loading reference structure
    ###############################################
    """

    if fpath is not None:
        directory = os.path.dirname(fpath)

        if os.getcwd() != directory:
            os.chdir(directory)

        file = os.path.basename(fpath)
        name = complex_name + '_ref'
        cmd.load(file, name)
        cmd.show_as('cartoon', complex_name + '_ref')
        cmd.set('cartoon_color', 'magenta', complex_name + '_ref')

    elif fname is not None:

        if path is not None:
            os.chdir(path)

        cmd.load(fname + '.pdb', complex_name + '_ref')
        cmd.show_as('cartoon', complex_name + '_ref')
        cmd.set('cartoon_color', 'magenta', complex_name + '_ref')

    elif pdb is not None:
        l = pdb.split(':')
        ref = l[0]

        cmd.fetch(ref, name = complex_name + '_ref')
        cmd.show_as('cartoon', complex_name + '_ref')
        cmd.set('cartoon_color', 'magenta', complex_name + '_ref')

        if len(l) > 1:
            chids = '+'.join(''.join(l[1:]))
            cmd.remove('ref and not chain %s' % chids)

    cmd.orient('all')


def create_sel(complex):

    """
    UNAVAILABLE FROM PYMOL COMMAND LINE

    :param complex: a Complex object representing the complex currently being loaded

    :return: creates string selections for the receptor and peptide of the complex from the 'complex' parameter
             saves them to the 'receptor_sel' and 'peptide_sel' attributes of the Complex object from the 'complex' parameter
    """

    receptor_sel = '('
    peptide_sel = '('

    for chain in complex.receptor_chains:
        receptor_sel += 'chain ' + chain
        if complex.receptor_chains.index(chain) != len(complex.receptor_chains) - 1:
            receptor_sel += ' or '
        else:
            receptor_sel += ')'

    for chain in complex.peptide_chains:
        peptide_sel += 'chain ' + chain
        if complex.peptide_chains.index(chain) != len(complex.peptide_chains) - 1:
            peptide_sel += ' or '
        else:
            peptide_sel += ')'

    complex.receptor_sel = receptor_sel
    complex.peptide_sel = peptide_sel


def create_obj_and_repr(complex, name):

    """
    UNAVAILABLE FROM PYMOL COMMAND LINE

    :param complex: a Complex object representing the complex currently being loaded

    :param name: name of the PyMOL object currently being created

    :return: creates hidden PyMOL objects for the receptor and peptide of the complex from the 'complex' parameter
             sets default representations to the receptor and peptide of the complex from the 'complex' parameter
    """

    from representations import default_repr

    cmd.create('_receptor_' + name, name + ' and ' + complex.receptor_sel)
    cmd.create('_peptide_' + name, name + ' and ' + complex.peptide_sel)

    cmd.disable('_receptor_' + name)
    cmd.disable('_peptide_' + name)

    default_repr(name)


cmd.extend('download_job', download_job)
cmd.extend('load_complex', load_complex)
cmd.extend('load_ref', load_ref)