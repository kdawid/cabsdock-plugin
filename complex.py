class Complex(object):

    def __init__(self, name, receptor_chains, peptide_chains):

        """
        :param name: name of the complex;
                     i.e.; 'my_job'

        :param receptor_chains: chain ids/id of the complex's receptor;
                                i.e., 'A B' - if receptor consists of two chains
                                i.e., 'A' - if receptor consists of one chain

        :param peptide_chains: chain id of the complex's peptide;
                               i.e., 'C'
        """

        self.name = name
        self.receptor_chains = receptor_chains
        self.peptide_chains = peptide_chains
        self.receptor_sel = str()
        self.peptide_sel = str()
        self.filenames = list()


def get_complex(model, complexes):

    """
    :param model: name of the model's PyMOL object;
                  i.e., 'my_job_model_1'

    :param complexes: global variable 'complexes'

    :return: a Complex object representing the complex to which the model from the 'model' parameter belongs
    """

    complex = None

    complex_name = (model.split('_'))[0]

    for c in complexes:
        if c.name == complex_name:
            complex = c
            break

    return complex