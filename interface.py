from pymol import cmd, stored
from interface_residues import interfaceResidues
from yrb import yrb
from initialization import complexes
from complex import get_complex


def show_interface(models,
                   representation = 'cartoon', cutoff = 1.):

    """
    :param models: name/names of the model's/models' PyMOL object/objects, separated by a space character (if more than one);
                   i.e., 'my_job_model_1' - if only one model provided
                         'my_job_model_1 my_job_model_7' - if two models provided

    :param representation: representation in which the model/models from the 'models' parameter will be shown;
                           - available: 'cartoon' - default
                                        'surface'

    :param cutoff: if distance between any two of receptor's and ligand's interface residues exceeds the 'cutoff' parameter,
                   these two residues will be dismissed;
                   - default: 1.0

    :return: sets representations to receptor, ligand and interface residues

    For more detailed information on parameters please go to https://bitbucket.org/kdawid/cabsdock-plugin/wiki/Home.
    """

    """
    ################################################################################################
                            Changing colors' values to their defaults
                      (important if YRB script has been executed beforehand)
    ################################################################################################
    """

    cmd.set_color('yellow', [1.0, 1.0, 0.0])
    cmd.set_color('grey', [0.5, 0.5, 0.5])
    cmd.set_color('red', [1.0, 0.0, 0.0])
    cmd.set_color('blue', [0.0, 0.0, 1.0])

    """
    ################################################################################################
    """


    models = models.split()
    leave_enabled = list()
    orient_selection = str()

    for model in models:
        '''if complex is None:
            complex = complexes[0]

        else:
            for c in complexes:
                if c.name == complex:
                    complex = c
                    break'''

        complex = get_complex(model, complexes)

        for obj in cmd.get_names('objects'):
            if model + '_if_hb' in obj:
                leave_enabled.append(obj)

        leave_enabled.append(model)

        if model + '_if' not in cmd.get_names('selections'):
            get_interface(model, complex, cutoff)

        """
        ######################################################
                  Creating complex's representation
        ######################################################
        """

        cmd.enable(model)
        cmd.hide('everything',
                 '(' + complex.receptor_sel + ' and ' + model + ') or (' + complex.peptide_sel + ' and ' + model + ')')

        cmd.show_as('cartoon', model)

        cmd.set('cartoon_color', 'white', complex.receptor_sel + ' and ' + model)
        cmd.set('cartoon_color', 'cyan', complex.peptide_sel + ' and ' + model)
        cmd.set('cartoon_loop_radius', 0.5)

        if representation == 'surface':
            cmd.enable('_receptor_' + model)
            cmd.enable('_peptide_' + model)

            cmd.show_as('surface', '_receptor_' + model)
            cmd.show_as('surface', '_peptide_' + model)
            cmd.set('surface_color', 'white', '_receptor_' + model)
            cmd.set('surface_color', 'cyan', '_peptide_' + model)
            cmd.set('transparency', 0.7)

            leave_enabled.append('_receptor_' + model)
            leave_enabled.append('_peptide_' + model)

        if len(models) > 1:
            cmd.set('transparency', 0.7)

        else:
            if representation == 'cartoon':
                cmd.set('transparency', 0)

        """
        ###################################################
                Creating interface's representation
        ###################################################
        """

        cmd.set('cartoon_side_chain_helper', 1)
        cmd.set('stick_radius', 0.1, model)

        cmd.show('sticks', model + '_rec_if')
        cmd.show('sticks', model + '_pept_if')

        cmd.color('green', model + '_rec_if')
        cmd.color('blue', model + '_pept_if')

        cmd.disable(model + '_if')
        cmd.disable(model + '_rec_if')
        cmd.disable(model + '_pept_if')

        """
        ####################################################
        """

        if models.index(model) == len(models) - 1:
            orient_selection += model + '_if'

        else:
            orient_selection += model + '_if or '

    """
    ##############################
             Cleaning up
    ##############################
    """

    for obj in cmd.get_names('objects'):
        if obj in leave_enabled:
            cmd.enable(obj)

        else:
            cmd.disable(obj)

    cmd.orient(orient_selection)


def show_yrb_interface(models,
                       representation = 'cartoon', cutoff = 1.):

    """
    :param models: name/names of the model's/models' PyMOL object/objects, separated by a space character (if more than one);
                   i.e., 'my_job_model_1' - if only one model provided
                         'my_job_model_1 my_job_model_7' - if two models provided

    :param representation: representation in which the model/models from the 'models' parameter will be shown;
                           - available: 'cartoon' - default
                                        'surface'

    :param cutoff: if distance between any two of receptor's and ligand's interface residues exceeds the 'cutoff' parameter,
                   these two residues will be dismissed;
                   - default: 1.0

    :return: creates 2 hidden PyMOL objects for each model from the 'models' parameter:
             - receptor's interface residues colored via YRB script
             - ligand's interface residues colored via YRB script;
             sets representations to receptor, ligand and interface residues

    For more detailed information on parameters please go to https://bitbucket.org/kdawid/cabsdock-plugin/wiki/Home.
    """

    """
    ################################################################################################
                        Changing colors' values according to YRB script
    ################################################################################################
    """

    cmd.set_color('yellow', [0.950, 0.78, 0.0])
    cmd.set_color('grey', [0.95, 0.95, 0.95])
    cmd.set_color('red', [1.0, 0.4, 0.4])
    cmd.set_color('blue', [0.2, 0.5, 0.8])

    """
    ################################################################################################
    """

    models = models.split()
    orient_selection = str()
    leave_enabled = list()
    hb_models = list()

    """
    ##################################################
            Preparing for executing YRB script
    ##################################################
    """

    for obj in cmd.get_names('objects'):
        if '_if_hb' in obj:
            hb_models.append(obj[:-6])
            cmd.delete(obj)

    """
    ##################################################
    """

    for model in models:
        complex = get_complex(model, complexes)

        leave_enabled.append(model)

        if model + '_if' not in cmd.get_names('selections'):
            get_interface(model, complex, cutoff)

        if '_yrb_' + model + '_rec_if' not in cmd.get_names('objects'):

            """
            ##################################################
                    Preparing for executing YRB script
            ##################################################
            """

            cmd.disable('all')

            cmd.create('_yrb_' + model + '_rec_if', model + '_rec_if')
            cmd.create('_yrb_' + model + '_pept_if', model + '_pept_if')

            """
            ##################################################
            """

            yrb()

        """
        #############################################################
                  Creating complex's default representation
        #############################################################
        """

        cmd.enable(model)

        cmd.hide('everything',
                 '(' + complex.receptor_sel + ' and ' + model + ') or (' + complex.peptide_sel + ' and ' + model + ')')

        cmd.show_as('cartoon', model)

        cmd.set('cartoon_color', 'white', complex.receptor_sel + ' and ' + model)
        cmd.set('cartoon_color', 'cyan', complex.peptide_sel + ' and ' + model)
        cmd.set('cartoon_loop_radius', 0.5)

        if len(models) > 1:
            cmd.set('transparency', 0.7)

        else:
            if representation == 'cartoon':
                cmd.set('transparency', 0)

        """
        ###################################################
                Creating interface's representation
        ###################################################
        """

        leave_enabled.append('_yrb_' + model + '_pept_if')
        leave_enabled.append('_yrb_' + model + '_rec_if')

        cmd.enable('_yrb_' + model + '_rec_if')
        cmd.enable('_yrb_' + model + '_pept_if')

        cmd.set('cartoon_side_chain_helper', 1)

        cmd.show_as('sticks', '_yrb_' + model + '_rec_if')
        cmd.show_as('sticks', '_yrb_' + model + '_pept_if')
        cmd.set('stick_radius', 0.1, '_yrb_' + model + '_rec_if')
        cmd.set('stick_radius', 0.23, '_yrb_' + model + '_pept_if')

        cmd.show('cartoon', '_yrb_' + model + '_rec_if')
        cmd.show('cartoon', '_yrb_' + model + '_pept_if')

        """
        ###################################################
        """

        if models.index(model) == len(models) - 1:
            orient_selection += model + '_if'

        else:
            orient_selection += model + '_if or '

    """
    #############################################################
             Creating complex's optional representation
    #############################################################
    """

    if representation == 'surface':

        for model in models:

            cmd.enable('_receptor_' + model)
            cmd.enable('_peptide_' + model)

            cmd.show_as('surface', '_receptor_' + model)
            cmd.show_as('surface', '_peptide_' + model)
            cmd.color('white', '_receptor_' + model)
            cmd.color('cyan', '_peptide_' + model)
            cmd.set('transparency', 0.7)

            leave_enabled.append('_receptor_' + model)
            leave_enabled.append('_peptide_' + model)

    """
    ##############################
             Cleaning up
    ##############################
    """

    for model in hb_models:
        show_interface_hbonds(model)

        if model in leave_enabled:
            leave_enabled.append(model + '_if_hb')

    for obj in cmd.get_names('objects'):
        if obj in leave_enabled:
            cmd.enable(obj)

        else:
            cmd.disable(obj)

    cmd.orient(orient_selection)


def get_interface(model, complex, cutoff):

    """
    UNAVAILABLE FROM PYMOL COMMAND LINE

    :param model: name of the model's PyMOL object;

    :param complex: name of the complex to which the model from the 'model' parameter belongs;

    :param cutoff: if distance between any two of receptor's and ligand's interface residues exceeds the cutoff parameter,
                   these two residues will be dismissed;

    :return: creates 3 selections for the model from the 'model' parameter:
             - receptor's interface residues
             - ligand's interface residues
             - model's interface residues, consisting of both receptor's and ligand's interface residues
    """

    interfaces_temp = list()
    interfaces_final = list()
    count = 1

    """
    ##############################################################################################
               Finding interface residues between every two receptor and ligand chains
    ##############################################################################################
    """

    for receptor_chain in complex.receptor_chains:
        for peptide_chain in complex.peptide_chains:
            if cutoff != 1.0:

                interfaces_temp.append(interfaceResidues(model, cA = 'c. ' + receptor_chain,
                                                         cB = 'c. ' + peptide_chain, cutoff = cutoff,
                                                         selName = 'temp_interface_' + str(count)))
            else:

                interfaces_temp.append(interfaceResidues(model, cA = 'c. ' + receptor_chain,
                                                         cB = 'c. ' + peptide_chain,
                                                         selName = 'temp_interface_' + str(count)))
            count += 1

    """
    ##############################################################################################
    """

    stored.model_interface = list()
    model_interface = model + ' and ('
    receptor_interface = model + ' and ('
    peptide_interface = model + ' and ('

    """
    #########################################################################################################
              Creating strings with model's, receptor's and ligand's interface residues selections
    #########################################################################################################
    """

    for sel in cmd.get_names('selections'):
        if 'temp_interface' in sel:
            cmd.iterate(sel,
                        'stored.model_interface.append("(chain " + chain + " and resi " + resi + " and name " + name + ") or ")')

    for aa_sel in stored.model_interface:
        model_interface += aa_sel

    model_interface = model_interface[:-4]
    model_interface += ')'

    for chain in complex.receptor_chains:
        for aa_sel in stored.model_interface:
            if 'chain ' + chain in aa_sel:
                receptor_interface += aa_sel

    receptor_interface = receptor_interface[:-4]
    receptor_interface += ')'

    for chain in complex.peptide_chains:
        for aa_sel in stored.model_interface:
            if 'chain ' + chain in aa_sel:
                peptide_interface += aa_sel

    peptide_interface = peptide_interface[:-4]
    peptide_interface += ')'

    """
    #########################################################################################################
    """

    cmd.select(model + '_rec_if', receptor_interface)
    cmd.select(model + '_pept_if', peptide_interface)
    cmd.select(model + '_if', model_interface)

    """
    ##############################
             Cleaning up
    ##############################
    """

    for i in interfaces_temp:
        for j in i:
            interfaces_final.append(j)

    for sel in cmd.get_names('selections'):
        if 'temp_interface' in sel:
            cmd.delete(sel)


def show_interface_hbonds(models):

    """
    :param models: name/names of the model's/models' PyMOL object/objects, separated by a space character (if more than one);
                   i.e., 'my_job_model_1' - if only one model provided
                         'my_job_model_1 my_job_model_7' - if two models provided

    :return: for each model from the 'models' parameter:
             - creates a PyMOL object for hydrogen bonds between receptor's and ligand's interface residues

    For more detailed information on parameters please go to https://bitbucket.org/kdawid/cabsdock-plugin/wiki/Home.
    """

    models = models.split()
    orient_selection = str()

    for model in models:

        if model + '_if_hb' in cmd.get_names('objects'):

            cmd.enable(model + '_if_hb')
            cmd.enable(model)

        else:

            """
            #####################################################
                    Creating an object for hydrogen bonds
            #####################################################
            """

            cmd.enable(model)
            cmd.distance(model + '_if_hb', model + '_rec_if', model + '_pept_if', mode = 2)
            cmd.hide('labels', model + '_if_hb')

            """
            #####################################################
            """

        if models.index(model) == len(models) - 1:
            orient_selection += model + '_if'

        else:
            orient_selection += model + '_if or '

    cmd.orient(orient_selection)


cmd.extend('show_interface', show_interface)
cmd.extend('show_interface_hbonds', show_interface_hbonds)
cmd.extend('show_yrb_interface', show_yrb_interface)
