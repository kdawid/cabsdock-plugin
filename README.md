# ABOUT THE PLUGIN #

A clear molecular visualization of three-dimensional structures of protein-peptide complexes is a necessity in their studies. Such visualizations can be performed using feature-rich PyMOL molecular graphics software, however using PyMOL may be troublesome for many scientists inexperienced in programming. Our goal was to create a PyMOL plugin which provides users, especially beginners and those without any programming background, with a simple analysis and visualization tool. Our plugin is adapted to work with PDB files and provides dedicated support for results produced with our protein-peptide docking method, [CABS-dock](https://bitbucket.org/lcbio/cabsdock). It comes with a set of default visualization options to choose from, as well as easy-to-use tools for analysis of the complex features and characteristics, such as several options for characterizing binding interface. The plugin may be used in the graphical interface mode for regular use, or as a command-line tool. 

## Detailed tutorial on how to use CABSdock plugin can be found on [CABSdock plugin Wiki page](https://bitbucket.org/kdawid/cabsdock-plugin/wiki/Home) ##

-------------------------------------------

# PREREQUISITES #

In order for the plugin to be able to make movies, **you have to have [ffmpeg](https://www.ffmpeg.org/) installed**. You can find **all of its releases [here](https://www.johnvansickle.com/ffmpeg/)** (we highly recommend to use **git builds**). Please **remember to copy the binaries into your ```$PATH```** - only then the plugin will be able to use ffmpeg program properly.

For example, **if you are using the plugin on Linux system**, the command for copying ffmpeg binaries into your ```$PATH``` should look as follows:

```
#!bash
sudo cp ff* qt-faststart /usr/local/bin/
```

-------------------------------------------

# PLUGIN INSTALLATION #

To install CABSdock plugin:

* **download latest version** of CABSdock plugin:

```
#!bash
git clone https://bitbucket.org/kdawid/cabsdock-plugin.git
```

* **run PyMOL** (from any directory):

```
#!bash
pymol
```

* go to **PyMOL Plugin Manager**:

![](https://biocomp.atlassian.net/secure/attachment/10309/install1.png)

* go to **Install New Plugin bookmark and click on 'Choose file...'**:

![](https://biocomp.atlassian.net/secure/attachment/10310/install2.png)

* **from** the earlier downloaded **cabsdock-plugin directory choose  \_\_init\_\_.py** file
* now **CABSdock plugin** should be **ready to use**.

------------------------------------------

Laboratory of Computational Biology, 2017