from pymol import cmd
from initialization import complexes
from complex import get_complex


def surface_surface(models):

    """
    :param models: names/a name of the models'/model's PyMOL objects/object, separated by a space character (if more than one);
                   i.e., 'my_job_model_1' - if only one model provided
                         'my_job_model_1 my_job_model_7' - if two models provided

    :return: sets a surface-surface representation to the model/models from the 'models' parameter

    For more detailed information on parameters please go to https://bitbucket.org/kdawid/cabsdock-plugin/wiki/Home.
    """

    models = models.split()
    objects = list()
    sele = str()

    """
    #######################################################################
                            Setting representations
    #######################################################################
    """

    for model in models:

        complex = get_complex(model, complexes)

        cmd.enable(model)
        cmd.enable('_receptor_' + model)
        cmd.enable('_peptide_' + model)

        cmd.hide('everything', '(' + complex.receptor_sel + ' and ' + model + ') or (' + complex.peptide_sel + ' and ' + model + ')')

        cmd.show_as('surface', '_receptor_' + model)
        cmd.show_as('surface', '_peptide_' + model)

        cmd.set('surface_color', 'white', '_receptor_' + model)
        cmd.set('surface_color', 'cyan', '_peptide_' + model)

        if len(models) > 1:
            cmd.set('transparency', 0.7)

        else:
            cmd.set('transparency', 0)

        objects.append('_receptor_' + model)
        objects.append('_peptide_' + model)
        objects.append(model)

        sele += model + ' '

    """
    ##############################
             Cleaning up
    ##############################
    """

    for obj in cmd.get_names('objects'):
        if obj not in objects:
            cmd.disable(obj)

    cmd.orient(sele)


def surface_cartoon(models):

    """
    :param models: names/a name of the models'/model's PyMOL objects/object, separated by a space character (if more than one);
                   i.e., 'my_job_model_1' - if only one model provided
                         'my_job_model_1 my_job_model_7' - if two models provided

    :return: sets a surface-cartoon representation to the model/models from the 'models' parameter

    For more detailed information on parameters please go to https://bitbucket.org/kdawid/cabsdock-plugin/wiki/Home.
    """

    models = models.split()
    objects = list()
    sele = str()

    """
    #######################################################################
                            Setting representations
    #######################################################################
    """

    for model in models:

        complex = get_complex(model, complexes)

        cmd.enable(model)
        cmd.enable('_receptor_' + model)
        cmd.hide('everything', '(' + complex.receptor_sel + ' and ' + model + ') or (' + complex.peptide_sel + ' and ' + model + ')')
        cmd.show_as('surface', '_receptor_' + model)
        cmd.show_as('cartoon', complex.peptide_sel)
        cmd.set('surface_color', 'white', '_receptor_' + model)
        cmd.set('cartoon_color', 'cyan', complex.peptide_sel)
        cmd.set('cartoon_loop_radius', 0.5)

        if len(models) > 1:
            cmd.set('transparency', 0.7)

        else:
            cmd.set('transparency', 0)

        objects.append('_receptor_' + model)
        objects.append(model)

        sele += model + ' '

    """
    ##############################
             Cleaning up
    ##############################
    """

    for obj in cmd.get_names('objects'):
        if obj not in objects:
            cmd.disable(obj)

    cmd.orient(sele)


def default_repr(model):

    """
    :param model: name of the model's PyMOL object;
                  i.e., 'my_job_model_1'

    :return: sets a default representation to the model from the 'model' parameter

    For more detailed information on parameters please go to https://bitbucket.org/kdawid/cabsdock-plugin/wiki/Home.
    """

    complex = get_complex(model, complexes)

    cmd.hide('surface')
    cmd.show_as('cartoon', complex.receptor_sel + ' and ' + model)
    cmd.set('cartoon_color', 'white', complex.receptor_sel + ' and ' + model)

    cmd.show_as('cartoon', complex.peptide_sel + ' and ' + model)
    cmd.set('cartoon_color', 'cyan', complex.peptide_sel + ' and ' + model)
    cmd.set('cartoon_loop_radius', 0.5)

    cmd.orient('all')


cmd.extend('surface_surface', surface_surface)
cmd.extend('surface_cartoon', surface_cartoon)
cmd.extend('default_repr', default_repr)